/* JMJ */
/**
 * Created by Jorge on 27/06/2015.
 */

angular.module('precationes', ['ngRoute']);// jshint ignore:line

angular.module('precationes').config(// jshint ignore:line
  function($routeProvider) {
    'use strict';
    $routeProvider
      .when('/', {
        templateUrl: 'templates/homeContent.html',
        controller: 'precationesController'
      })
      .when('/orationesUtilissimae/signumCrucis', {
        templateUrl: 'templates/signumCrucisContent.html',
        controller: 'precationesController'
      })
      .when('/orationesUtilissimae/perSignumCrucis', {
        templateUrl: 'templates/perSignumCrucisContent.html',
        controller: 'precationesController'
      })
      .when('/orationesUtilissimae/paterNoster', {
        templateUrl: 'templates/paterNosterContent.html',
        controller: 'precationesController'
      })
      .when('/orationesUtilissimae/aveMaria', {
        templateUrl: 'templates/aveMariaContent.html',
        controller: 'precationesController'
      })
      .when('/orationesUtilissimae/doxologiaMinor', {
        templateUrl: 'templates/doxologiaMinorContent.html',
        controller: 'precationesController'
      })
      .when('/matutinas/matinaPredica', {
        templateUrl: 'templates/matinaPredicaContent.html',
        controller: 'precationesController'
      })
      .when('/vespertinas/vesperasPredica', {
        templateUrl: 'templates/vesperasPredicaContent.html',
        controller: 'precationesController'
      });
  }
);

angular.module('precationes').controller(// jshint ignore:line
  'precationesController',
  [
    '$scope', '$location', 'precationesService',
    function($scope, $location, precationesService) {
      'use strict';

      $scope.menuStructure = {};

      precationesService.getJMJSidebarMenuStructure().then(
        function(data) {
          if (data.hasOwnProperty('jmjSidebarMenu')) {
            //noinspection JSUnresolvedVariable
            $scope.menuStructure = data.jmjSidebarMenu;
          }
        }
      );
    }
  ]
);

angular.module('precationes').service(// jshint ignore:line
  'precationesService',
  ['$http', '$q', function($http, $q) {
    'use strict';

    // ---
    // PUBLIC METHODS.
    // ---

    // I get all of the friends in the remote collection.
    function getJMJSidebarMenuStructure() {

      var request = $http({
        method: 'get',
        url: 'assets/json/jmjSidebarMenu.json',
        params: {
          action: 'get'
        }
      });

      return request.then(handleSuccess, handleError) ;
    }

    // ---
    // PRIVATE METHODS.
    // ---

    // I transform the error response, unwrapping the application dta from
    // the API response payload.
    function handleError(response) {

      // The API response from the server should be returned in a
      // normalized format. However, if the request was not handled by the
      // server (or what not handles properly - ex. server error), then we
      // may have to normalize it on our end, as best we can.
      if (
        !angular.isObject(response.data) ||// jshint ignore:line
        !response.data.message
      ) {
        return $q.reject('An unknown error occurred.');
      }

      // Otherwise, use expected error message.
      return $q.reject(response.data.message);
    }

    // I transform the successful response, unwrapping the application data
    // from the API response payload.
    function handleSuccess(response) {
      return response.data;
    }

    return {
      getJMJSidebarMenuStructure: getJMJSidebarMenuStructure
    };

  }])
;