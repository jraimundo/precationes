/* JMJ */
/**
 * Created by Jorge on 27/06/2015.
 */

angular.module('precationes').directive(// jshint ignore:line
  'jmjSidebarMenu', function() {'use strict';
    return {
      restrict: 'E',
      scope: {
        menuStructure: '='
      },
      templateUrl: 'templates/jmjSidebarMenu.html'
    };
  }
);